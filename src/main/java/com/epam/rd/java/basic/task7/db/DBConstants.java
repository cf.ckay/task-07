package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
    public static final String FIND_ALL_USERS = "SELECT * FROM users u ORDER BY u.login";
    public static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
    public static final String DELETE_USERS = "DELETE FROM users WHERE login=?";
    public static final String GET_USER = "SELECT * FROM users u WHERE u.login=? ORDER BY u.login";

    public static final String FIND_ALL_TEAM = "SELECT * FROM teams t ORDER BY t.name";
    public static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE name=?";
    public static final String GET_TEAM = "SELECT * FROM teams t WHERE t.name=?  ORDER BY t.name";
    public static final String UPDATE_TEAMS = "UPDATE teams SET name=? WHERE id=?";

    public static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";
    public static final String GET_USER_TEAMS = "SELECT * FROM teams WHERE teams.id = ANY (SELECT team_id FROM users_teams WHERE user_id=?)";
}
