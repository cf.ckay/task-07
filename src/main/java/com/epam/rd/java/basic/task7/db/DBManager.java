package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	private static String getURL() {
		Properties properties = new Properties();
		try (InputStream in = new FileInputStream("app.properties")) {
			properties.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return properties.getProperty("connection.url");
	}

	private static Connection getConnection(String connectionUrl) throws DBException {
		try {
			return DriverManager.getConnection(connectionUrl);
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> userList = new ArrayList<>();
		try (Connection con = getConnection(getURL());
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery(DBConstants.FIND_ALL_USERS)) {

			while (rs.next()) {
				userList.add(getUserResSet(rs));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.INSERT_USER,
					 Statement.RETURN_GENERATED_KEYS)) {

			preps.setString(1, user.getLogin());

			if (preps.executeUpdate() == 0) {
				return false;
			}

			try (ResultSet rs = preps.getGeneratedKeys()) {
				if (rs.next()) {
					user.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		boolean isDeleteUsers = true;
		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.DELETE_USERS)) {

			for (User user : users) {
				preps.setString(1, user.getLogin());
				isDeleteUsers = isDeleteUsers && preps.executeUpdate() > 0;
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return isDeleteUsers;
	}

	public User getUser(String login) throws DBException {
		User user = null;

		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.GET_USER)) {

			preps.setString(1, login);

			try (ResultSet rs = preps.executeQuery()) {
				while (rs.next()) {
					user = getUserResSet(rs);
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = null;

		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.GET_TEAM)) {

			preps.setString(1, name);

			try (ResultSet rs = preps.executeQuery()) {
				while (rs.next()) {
					team = getTeamResSet(rs);
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teamList = new ArrayList<>();
		try (Connection con = getConnection(getURL());
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery(DBConstants.FIND_ALL_TEAM)) {

			while (rs.next()) {
				teamList.add(getTeamResSet(rs));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.INSERT_TEAM,
					 Statement.RETURN_GENERATED_KEYS)) {

			preps.setString(1, team.getName());

			if (preps.executeUpdate() == 0) {
				return false;
			}

			try (ResultSet rs = preps.getGeneratedKeys()) {
				if (rs.next()) {
					team.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teamList = new ArrayList<>();

		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.GET_USER_TEAMS)) {

			preps.setInt(1, user.getId());

			try (ResultSet rs = preps.executeQuery()) {
				while (rs.next()) {
					teamList.add(getTeamResSet(rs));
				}
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		boolean isDeleteTeam;

		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.DELETE_TEAM)) {

			preps.setString(1, team.getName());

			isDeleteTeam = preps.executeUpdate() > 0;

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return isDeleteTeam;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean isUpdateTeam;
		try (Connection con = getConnection(getURL());
			 PreparedStatement preps = con.prepareStatement(DBConstants.UPDATE_TEAMS)) {

			preps.setString(1, team.getName());
			preps.setInt(2, team.getId());

			isUpdateTeam = preps.executeUpdate() > 0;

		} catch (SQLException e) {
			throw new DBException(e.getMessage(), e);
		}
		return isUpdateTeam;
	}

	private Team getTeamResSet(ResultSet rs) throws SQLException {
		Team team = new Team();
		team.setId(rs.getInt("id"));
		team.setName(rs.getString("name"));
		return team;
	}

	private User getUserResSet(ResultSet rs) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("id"));
		user.setLogin(rs.getString("login"));
		return user;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement preps = null;

		try {
			con = getConnection(getURL());
			con.setAutoCommit(false);
			con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			preps = con.prepareStatement(DBConstants.SET_TEAMS_FOR_USER);

			for (Team team : teams) {
				preps.setInt(1, user.getId());
				preps.setInt(2, team.getId());
				preps.executeUpdate();
			}

			con.commit();

		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException(e.getMessage(), e);
		} finally {
			try {
				autoClose(con);
				autoClose(preps);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	private void autoClose(AutoCloseable ac) throws Exception {
		if (ac != null) {
			ac.close();
		}
	}
}